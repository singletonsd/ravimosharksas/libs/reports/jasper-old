#!/bin/bash

# Load environment variables from .env file
set -o allexport
source .env
set +o allexport

# Path to the XML file
xml_file="src/adapters/Ravimishark SAV Prod DataAdapter.jrdax"

# Check if required variables are set
if [[ -z "$SAV_PROD_DB_USER" || -z "$SAV_PROD_DB_PASS" ]]; then
  echo "Error: SAV_PROD_DB_USER or SAV_PROD_DB_PASS is not set in the .env file"
  exit 1
fi

# Check if the XML file exists
if [[ ! -f "$xml_file" ]]; then
  echo "Error: XML file '$xml_file' does not exist."
  exit 1
fi

# Use sed to replace the placeholder values in the XML
# Use sed to replace the values inside the XML tags
# The `sed -i` flag for macOS and Linux is handled differently
if [[ "$OSTYPE" == "darwin"* ]]; then
  # macOS (BSD sed)
  sed -i '' "s|<username>.*</username>|<username>${SAV_PROD_DB_USER}</username>|" "$xml_file"
  sed -i '' "s|<password>.*</password>|<password>${SAV_PROD_DB_PASS}</password>|" "$xml_file"
else
  # Linux (GNU sed)
  sed -i "s|<username>.*</username>|<username>${SAV_PROD_DB_USER}</username>|" "$xml_file"
  sed -i "s|<password>.*</password>|<password>${SAV_PROD_DB_PASS}</password>|" "$xml_file"
fi

echo "XML file updated successfully."
